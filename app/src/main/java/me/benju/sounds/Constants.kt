package me.benju.sounds

/**
 * The iTunes search API
 * https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/
 *
 * this api doesn't give the charts, we need the top 100 songs
 * use the rss : https://itunes.apple.com/fr/rss/topsongs/limit=100/json
 */
const val baseUrl = "https://itunes.apple.com/"