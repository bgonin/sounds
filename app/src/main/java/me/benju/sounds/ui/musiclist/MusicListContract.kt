package me.benju.sounds.ui.musiclist

import me.benju.sounds.model.rss.TopSongs
import me.benju.sounds.model.search.SearchResult
import me.benju.sounds.ui.BasePresenter
import me.benju.sounds.ui.BaseView

interface MusicListContract {

    interface View : BaseView<Presenter> {
        fun setTopSongsList(result: TopSongs)
        fun setSearchResultsList(result: SearchResult)
        fun showErrorMessage()
        fun hideProgress()
        fun showProgress()
    }

    interface Presenter : BasePresenter {
        fun getTop100()
        fun searchMusic(searchQuery: String)
    }

}