package me.benju.sounds.ui

interface BasePresenter {
    fun start()
}