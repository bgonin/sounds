package me.benju.sounds.ui

interface BaseView<T> {
    fun setPresenter(presenter: T)
}